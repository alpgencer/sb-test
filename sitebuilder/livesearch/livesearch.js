/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function showResult(str,id) {
	if (str.length==0) {
		document.getElementById("livesearch_"+id).innerHTML="";
		document.getElementById("livesearch_"+id).style.border="0px";
  		return;
  	}
	if (window.XMLHttpRequest) {
		// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {
		// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function()  {
		if (xmlhttp.readyState==4 && xmlhttp.status==200) {
		    document.getElementById("livesearch_"+id).innerHTML=xmlhttp.responseText;
		    document.getElementById("livesearch_"+id).style.border="1px solid #A5ACB2";
	    }
	}
	xmlhttp.open("GET","sitebuilder/livesearch/livesearch.php?id="+id+"&q="+str,true);
	xmlhttp.send();
}

function fillBox(fid,val) {
	var x=document.getElementById(fid);
	x.value=val;
	showResult('',fid);
}


