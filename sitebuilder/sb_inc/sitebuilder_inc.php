<?php
/**********************************************************
 Sitebuilder
 © 2010-2017 Alp H. Gencer
 All rights reserved. No duplication permitted.
 **********************************************************/
function token($tid, $l= "") {
	global $lang;
	if (!$l)
		if (isset( $lang ))
			$l= $lang;
		else
			$l= PRIMARY_LANGUAGE;
	$q= "SELECT $l FROM sb_tokens WHERE ID=$tid";
	$etext= dbGetVal( $q );
	if (is_null( $etext ))
		return;
	if (empty( $etext )) {
		$q= "SELECT " . PRIMARY_LANGUAGE . " FROM sb_tokens WHERE ID=$tid";
		$etext= dbGetVal( $q );
	}
	$res= preg_replace_callback( '|@(\d+)@|', function ($match) {
		return token( $match [1] );
	}, $etext );
	return $res;
}
function sbGetTokenID($en) {
	$q= "SELECT ID FROM sb_tokens WHERE {" . PRIMARY_LANGUAGE . "} LIKE '$en' LIMIT 1";
	return dbGetVal( $q );
}
function sbShowError($token, $err= 1, $subst= "") {
	if (is_int( $token ))
		$etext= token( $token );
	elseif (is_string( $token ))
		$etext= $token;
	else
		return;
	if ($err) {
		echo "<p><font color=#ff0000><b>";
	} else {
		echo "<p><font color=#008000><b>";
	}
	eval( "echo \"$etext\";" );
	echo "</b></font></p>\n";
	return;
}
function sbLocalText($text) {
	global $lang;
	if (strcmp( $lang, PRIMARY_LANGUAGE )) {
		$id= sbGetTokenID( $text );
		if (is_null( $id ))
			return $text;
		else
			return token( $id );
	} else {
		return $text;
	}
}
function sbScriptIncludes() {
	// includes for jQuery Datepicker
	echo '
<!-- SiteBuilder required script and CSS -->
	<script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>
	<script type="text/javascript" src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<script type="text/javascript" src="/sitebuilder/livesearch/livesearch.js"></script>
';
}

?>
