<?php
/**********************************************************
 Sitebuilder
 © 2010-2017 Alp H. Gencer
 All rights reserved. No duplication permitted.
 **********************************************************/
function sbShowForm($tag, $oid, $display= 0) {
	global $adminlevel, $TMP;
	if (isset( $_POST ['subdelete'] ))
		return;
	$q= "SELECT * FROM sb_forms WHERE TAG='$tag'";
	$form= dbGetRow( $q );
	$formid= $form ['ID'];
	$table= $form ['TABLE'];
	$dellevel= $form ["DELLEVEL"];

	$q= "SELECT FIELD FROM sb_formfields WHERE FORMID=$formid ORDER BY ORD LIMIT 1";
	$idfield= dbGetVal( $q );

	if ($oid >= 0) {
		$q= "SELECT * FROM $table WHERE $idfield='$oid'";
		$values= dbGetRow( $q );
	}

	$q= "SELECT * FROM sb_formfields WHERE FORMID=$formid ORDER BY ORD";
	$forms= dbQuery( $q );

	if (!$display)
		echo '<form action="" method="post" enctype="multipart/form-data">';
	echo "<p><table border=0 cellspacing=0 cellpadding=3>\n";

	foreach ( $forms as $form ) {
		$field= $form ["FIELD"];
		$type= $form ["TYPE"];
		if (TOKENIZED)
			$prompt= token( $form ["PROMPTTOKEN"] );
		else
			$prompt= $form ["PROMPT"];
		if (isset( $_POST [$field] )) {
			$default= $_POST [$field];
			/*
			 * if($type==5) {
			 * $prop=explode(" ",$form["PROPS"]));
			 * $val=$_POST[$field];
			 * $q="SELECT $prop[1] from $prop[0] WHERE $prop[2]='$val'";
			 * $default=dbGetVal($q);
			 * }
			 */
		} elseif ($oid < 0) {
			$default= $form ["DEFAULT"];
		} else {
			$default= $values [$field];
			$_POST [$field]= $values [$field];
		}
		$default= htmlspecialchars( $default );

		switch ($type) {
			case 0 :
				if (!$display)
					echo "<input type=\"hidden\" name=\"$field\" value=\"$default\">\n";
				break;
			case 1 :
				$width= $form ["PROPS"];
				echo "<tr><td><b>$prompt:</b></td>\n";
				if (!$display)
					echo "<td><input type=\"text\" name=\"$field\" maxlength=$width size=$width value=\"$default\"></td></tr>\n";
				else
					echo "<td>$default</td></tr>\n";
				break;
			case 2 :
				echo "<tr><td><b>$prompt:</b></td><td>\n";
				$q= $form ["PROPS"];
				eval( "\$q=\"$q\";" );
				$result= dbQuery( $q );
				if (is_null( $result ) || $result === FALSE)
					return;
				if (!$display)
					echo "<select name=\"$field\">\n";
				foreach ( $result as $row ) {
					$cid= $row ["ID"];
					$cval= $row ["VAL"];
					if (!$display) {
						echo "<option value=\"$cid\"";
						if ($cid == $default)
							echo " selected=\"selected\"";
						echo ">$cval</option>\n";
					} elseif ($cid == $default)
						echo "$cval";
				}
				if (!$display)
					echo "</select>";
				echo "</td></tr>\n";
				break;
			case 3 :
				if (!strcmp( $default, "today" ))
					$default= date( 'Y-m-d' );
				echo "<tr><td><b>$prompt:</b></td><td>\n";
				if (!$display) {
					echo '<script>
 						$( function() {
    						$( "#' . $field . '" ).datepicker({
    							changeMonth: true,
      							changeYear: true,
						      	dateFormat : "yy-mm-dd",
      							showOn: "focus",
								firstDay: 1, 
								monthNamesShort: [ "Ocak","Şubat","Mart","Nisan","Mayıs","Haziran","Temmuz","Ağustos","Eylül","Ekim","Kasım","Aralık" ],
      							defaultDate: "' . $default . '"
     						});
  						} );
  					</script>';
					echo "\n<input type=\"text\" id=\"$field\" name=\"$field\" value=\"$default\" maxlength=\"10\">\n";
				} else if ($default)
					echo date( "j.m.Y", strtotime( $default ) );
				echo "</td></tr>\n";
				break;
			case 4 :
				$rows= $form ["PROPS"];
				echo "<tr><td colspan=2><b>$prompt:</b></td></tr><tr><td colspan=2>\n";
				if (!$display)
					echo "<textarea name=\"$field\" rows=\"$rows\" cols=\"80\">$default</textarea>";
				else
					echo str_replace( "\n", "<br>", $default );
				echo "</td></tr>\n";
				break;
			case 5 :
				$fid= $form ["ID"];
				$prop= explode( " ", $form ["PROPS"] );
				$q= "SELECT $prop[2] FROM $prop[0] WHERE $prop[1]='$default'";
				$default= dbGetVal( $q );
				echo "<tr><td><b>$prompt:</b></td>\n";
				if (!$display)
					echo "<td><input type=\"text\" name=\"$field\" id=\"$fid\" maxlength=$prop[3] size=$prop[3] value=\"$default\" onkeyup=\"showResult(this.value,$fid)\">\n<div id=\"livesearch_$fid\"></div></td></tr>\n";
				else
					echo "<td>$default</td></tr>\n";
				break;
			case 6 :
				echo "<tr>";
				if (!$display) {
					echo "<input type=\"hidden\" value=\"0\" name=\"$field\">";
					echo "<td><input type=\"checkbox\" name=\"$field\" value=\"1\"";
					if ($default)
						echo " checked";
					echo "></td>";
					echo "<td>$prompt</td>\n";
				} elseif ($default)
					echo "<td colspan=2>$prompt</td>\n";
				echo "</tr>\n";
				break;
			case 7 :
				echo "<tr><td><b>$prompt:</b></td><td>\n";
				$q= $form ["PROPS"];
				eval( "\$q=\"$q\";" );
				$result= dbQuery( $q );
				if (is_null( $result ))
					return;
				foreach ( $result as $row ) {
					$cid= $row ["ID"];
					$cval= $row ["VAL"];
					if (!$display) {
						echo "<input type=\"radio\" name=\"$field\" value=\"$cid\"";
						if ($cid == $default)
							echo " checked";
						echo ">$cval<br>\n";
					} elseif ($cid == $default)
						echo $cval;
				}
				echo "</td></tr>\n";
				break;
		}
	}

	if (!$display) {
		echo "<tr><td></td>\n";
		echo '<td style="text-align: right;"><input class="button round" type="submit" name="submit" value="' . token( 99 ) . '"></td>';
		echo "<tr>\n";
		if ($oid > 0 && $adminlevel >= $dellevel) {
			echo '<tr><td><input class="button alert tiny" type="submit" name="subdelete" value="' . token( 98 ) . '" onClick="javascript: return confirm(\'' . token( 97 ) . '\');"></td>';
			echo "</tr>\n";
		}
	}
	echo "</table>\n";
	if (!$display)
		echo "</form>\n";
}
function sbProcessForm($tag, $oid, $debugflag= 0) {
	global $sbPagePath;

	if (isset( $_POST ['submit'] ) || isset( $_POST ['subdelete'] ) || isset( $_POST ['subdoc'] )) {
		$q= "SELECT * FROM sb_forms WHERE TAG='$tag'";
		$form= dbGetRow( $q );
		$formid= $form ['ID'];
		$table= $form ['TABLE'];
		$object= $tag;

		$q= "SELECT FIELD FROM sb_formfields WHERE FORMID=$formid ORDER BY ORD LIMIT 1";
		$idfield= dbGetVal( $q );

		if (isset( $_POST ['subdelete'] )) {
			dbDelete( $table, "$idfield=$oid" );
			sbShowError( 202, 0, $object );
			echo "<meta http-equiv=\"refresh\" content=\"5;url=?p=$sbPagePath&form=$tag&id=-1\">";
			return $oid;
		}

		$q= "SELECT * FROM sb_formfields WHERE FORMID=$formid ORDER BY ORD";
		$forms= dbQuery( $q );
		if (is_null( $forms )) {
			echo "Incorrect form";
			return;
		}
		$fieldsvalues= [ ];
		foreach ( $forms as $form ) {
			$fieldtype= $form ["TYPE"];
			$field= $form ["FIELD"];
			if ($fieldtype == 5) {
				$prop= explode( " ", $form ["PROPS"] );
				$val= $_POST [$field];
				$q= "SELECT $prop[1] FROM $prop[0] WHERE $prop[2]='$val'";
				$_POST [$field]= dbGetVal( $q );
			}
			if ($fieldtype == 3 && !$_POST [$field]) {
				// Handle empty date fields
				$fieldsvalues [$field]= '0000-00-00';
			} elseif (strncmp( $field, "TMP", 3 )) {
				// If not a temp field, add to the list of fields to set.
				$fieldsvalues [$field]= $_POST [$field];
			}
		}

		if ($oid < 0 || is_null( $oid )) {
			/* Add the new object to the database */
			$q= "SELECT MAX($idfield) FROM $table";
			$maxid= dbGetVal( $q );
			$oid= $maxid + 1;
			$fieldsvalues [$idfield]= $oid;
			dbInsert( $table, $fieldsvalues, $debugflag );
			sbShowError( 203, 0, $object );
			if (isset( $_POST ['submit'] ))
				echo "<meta http-equiv=\"refresh\" content=\"3;url=?p=$sbPagePath&form=$tag&id=$oid\">";
		} else {
			/* Change existing data */
			dbUpdate( $table, $fieldsvalues, "$idfield=$oid", $debugflag );
			sbShowError( 204, 0, $object );
			/*
			 * if(isset($_POST['submit']))
			 * echo "<meta http-equiv=\"refresh\" content=\"3;url=?p=$sbPagePath&form=$tag&id=$oid\">";
			 */
		}
	}
	return $oid;
}

?>
