<?php
/**********************************************************
 Sitebuilder
 © 2010-2020 Alp H. Gencer
 All rights reserved. No duplication permitted.
 **********************************************************/
function sbNormalizePath($path) {
	global $sbSitePath;
	if ($path [0] != '/') {
		$path= $sbSitePath . "/" . $path;
	}
	return $path;
}
function sbGetSubPages($parent, $pages, $explodedpath) {
	$tmp= $parent;
	foreach ( $pages as $key => $value ) {
		if ($key != "children") {
			$tmp [$key]= $value;
		}
	}
	if (isset( $pages ['children'] )) {
		$node= $explodedpath [0];
		if (isset( $pages ['children'] [$node] ))
			return sbGetSubPages( $tmp, $pages ['children'] [$node], array_slice( $explodedpath, 1 ) );
		else
			return NULL;
	} else {
		return $tmp;
	}
}
function sbGetPage($path) {
	global $sbPages;
	$result= array ();
	return sbGetSubPages( $result, $sbPages, array_slice( explode( "/", $path ), 1 ) );
}
function sbShowPage() {
	global $adminlevel, $sbPage, $sbPagePath;

	if ($sbPage == NULL)
		return;
	if ($sbPage ['adminlevel'] > A_DEFAULT && $adminlevel == A_DEFAULT) {
		$_SESSION ['REDIRECT']= $_SERVER ['REQUEST_URI'];
		echo '<meta http-equiv="refresh" content="0;url=?p=' . PAGE_LOGIN . '">';
		return;
	} elseif ($sbPage ['adminlevel'] > $adminlevel)
		return;
	if (TOKENIZED)
		$title= token( $sbPage ['title'] );
	else
		$title= $sbPage ['title'];
	echo "
		<h4 id=\"page-title\">$title</h4>
	 	" . "\n";
	include ("code/$sbPagePath.php");
}
function sbLinkToPage($page, $options= "", $ttext= "", $newwindow= 0) {
	$path= sbNormalizePath( $page );
	if ($ttext == "") {
		$p= sbGetPage( $path );

		if (is_null( $p )) {
			// sbShowError(201,1,$page);
			return;
		}
		if (TOKENIZED)
			$ttext= token( $p ["title"] );
		else
			$ttext= $p ["title"];
	}
	echo str_repeat( "\t", 3 ) . "<p><a href=\"?p=$path";
	if ($options)
		echo "&$options";
	echo '"';
	if ($newwindow)
		echo ' target="_blank"';
	echo ">$ttext</a></p>\n";
}

$sbPages= json_decode( file_get_contents( "code/pages.json" ), true );
// Set current page
if (isset( $_GET ['p'] ))
	$sbPagePath= $_GET ['p'];
else
	$sbPagePath= PAGE_DEFAULT;
if (is_null( sbGetPage( $sbPagePath ) ))
	$sbPagePath= PAGE_DEFAULT;
$sbSitePath= dirname( $sbPagePath );
$sbPage= sbGetPage( $sbPagePath );
?>
