<?php
/**********************************************************
 Sitebuilder
 © 2010-2017 Alp H. Gencer
 All rights reserved. No duplication permitted.
 **********************************************************/
function sbShowList($tag, $qcriteria= "1", $linkref= "", $pageoptions= "", $debugflag= 0) {
	global $sbPagePath;

	$q= "SELECT * FROM sb_lists WHERE TAG='$tag'";
	$listdata= dbGetRow( $q );
	$listid= $listdata ['ID'];
	$maintable= $listdata ['MAINTABLE'];
	if ($listdata ['SHOWID'])
		$jstart= 0;
	else
		$jstart= 1;
	$object= $listdata ['TAG'];

	if (isset( $_GET ['sorder'] ))
		$sorder= $_GET ['sorder'];
	else
		$sorder= $listdata ['DEFSORT'];

	$q= "SELECT * FROM sb_listfields WHERE LISTID=$listid ORDER BY ORD";
	$listfields= dbQuery( $q );
	if (is_null( $listfields )) {
		echo "Error with list fields";
		return;
	}

	/* Build query */
	$tables []= $maintable;
	$criteria []= $qcriteria;
	if ($linkref)
		echo str_repeat( "\t", 2 ) . '<table class="listTable hoverTable">' . "\n";
	else
		echo str_repeat( "\t", 2 ) . '<table class="listTable">' . "\n";

	$numfields= $listfields->rowCount();
	$listfields= $listfields->fetchAll();
	echo str_repeat( "\t", 3 ) . "<thead>" . "\n";
	echo str_repeat( "\t", 4 ) . "<tr>" . "\n";
	for($i= 0; $i < $numfields; $i++) {
		$table= $listfields [$i] ["TABLE"];
		$field= $listfields [$i] ["FIELD"];
		if (TOKENIZED)
			$fieldname= token( $listfields [$i] ["COLUMNTOKEN"] );
		else
			$fieldname= $listfields [$i] ["COLUMN"];
		if (strcmp( $maintable, $table )) {
			$fmain= $listfields [$i] ["LINKFMAIN"];
			$fchild= $listfields [$i] ["LINKFCHILD"];
			$tables []= "$table AS T$i";
			$fields []= "T$i.$field";
			$tname= "T$i";
			$criteria []= "$maintable.$fmain=T$i.$fchild";
		} else {
			$fields []= "$table.$field";
			$tname= $table;
		}
		if ($i >= $jstart) {
			echo str_repeat( "\t", 5 ) . "<th><a href=\"?p=$sbPagePath&sorder=$tname.`$field`&$pageoptions\">$fieldname</a></th>" . "\n";
		}
	}
	echo str_repeat( "\t", 4 ) . "</tr>" . "\n";
	echo str_repeat( "\t", 3 ) . "</thead>" . "\n";

	$listquery= sprintf( "SELECT %s from %s WHERE %s ORDER BY $sorder", implode( ", ", $fields ), implode( ", ", $tables ), implode( " AND ", $criteria ) );
	if ($debugflag)
		echo $listquery;

	$result= dbQuery( $listquery );
	if (is_null( $result )) {
		echo str_repeat( "\t", 2 ) . "</table>" . "\n";
		dbLog( $listquery, "E" );
		echo "No result returned using the query: ";
		echo str_repeat( "\t", 2 ) . "<code>$listquery</code>" . "\n";
		return;
	}

	echo str_repeat( "\t", 3 ) . "<tbody>" . "\n";
	foreach ( $result as $row ) {
		$iid= $row [0];
		if ($linkref != "") {
			$url= "?p=" . sbNormalizePath( $linkref ) . "&id=$iid";
			echo str_repeat( "\t", 4 ) . "<tr onclick=\"window.location.href='$url';\">";
		} else
			echo str_repeat( "\t", 4 ) . "<tr>";
		for($j= $jstart; $j < $numfields; $j++) {
			if ($listfields [$j] ["ISDATE"])
				$d= dateFormat( $row [$j] );
			else
				$d= substr( nl2br( htmlspecialchars( $row [$j] ) ), 0, 120 );
			echo "<td>$d</td>";
		}
		// if($linkref<>"") echo "<td><a href=\"$url\"><img src=\"images/icon_edit.gif\" alt=\"Edit\"></a></td>";
		echo "</tr>" . "\n";
	}
	echo str_repeat( "\t", 3 ) . "</tbody>" . "\n";

	echo str_repeat( "\t", 2 ) . "</table>\n";
	echo "\n";

	$num_rows= $result->rowCount();
	echo str_repeat( "\t", 2 ) . "<p># $object: $num_rows</p>" . "\n";
	echo "\n";
}

?>