<?php
/**********************************************************
 Sitebuilder 
 © 2010-2017 Alp H. Gencer
 All rights reserved. No duplication permitted.
 **********************************************************/
class MySQLDB extends PDO {

	/* Class constructor */
	function __construct() {
		try {
			PDO::__construct( 'mysql:host=' . DB_SERVER . ';dbname=' . DB_NAME . ';charset=utf8', DB_USER, DB_PASS );
		} catch ( PDOException $e ) {
			echo 'Connection failed: ' . $e->getMessage();
		}
	}

	// Log a query
	function dbLog($query, $type) {
		$date= date( 'Y-m-d H:i:s' );
		if (isset( $_SESSION ['ID'] ))
			$id= $_SESSION ['ID']; // somebody is logged in
		else
			$id= 0; // nobody is logged in
		$q2= PDO::quote( $query );
		$uri= PDO::quote( $_SERVER ['QUERY_STRING'] );
		$q= "INSERT INTO log_db (UID,TIME,TYPE,PAGE,QUERY) VALUES ($id ,'$date','$type',$uri,$q2)";
		$rows= $this->exec( $q );
	}

	/* backup the db OR just a table */
	function backup_tables($tables= '*', $filename) {
		$this->setAttribute( PDO::ATTR_ORACLE_NULLS, PDO::NULL_NATURAL );

		// Script Variables
		$compression= true;

		// create/open files
		if ($compression) {
			$zp= gzopen( $filename . '.sql.gz', "a9" );
		} else {
			$handle= fopen( $filename . '.sql', 'a+' );
		}

		// array of all database field types which just take numbers
		$numtypes= array (
				'tinyint',
				'smallint',
				'mediumint',
				'int',
				'bigint',
				'float',
				'double',
				'decimal',
				'real'
		);

		// get all of the tables
		if ($tables == "*") {
			$tables= [ ];
			$pstm1= $this->query( 'SHOW TABLES' );
			while ( $row= $pstm1->fetch( PDO::FETCH_NUM ) ) {
				array_push( $tables, $row [0] );
			}
		} else {
			$tables= is_array( $tables ) ? $tables : explode( ',', $tables );
		}

		// cycle through the table(s)
		foreach ( $tables as $table ) {
			echo "<p> Exporting $table</p>\n";
			$result= $this->query( "SELECT * FROM $table" );
			$num_fields= $result->columnCount();
			$num_rows= $result->rowCount();

			$return= "";
			// uncomment below if you want 'DROP TABLE IF EXISTS' displayed
			$return.= 'DROP TABLE IF EXISTS `' . $table . '`;';

			// table structure
			$pstm2= $this->query( "SHOW CREATE TABLE $table" );
			$row2= $pstm2->fetch( PDO::FETCH_NUM );
			$ifnotexists= str_replace( 'CREATE TABLE', 'CREATE TABLE IF NOT EXISTS', $row2 [1] );
			$return.= "\n\n" . $ifnotexists . ";\n\n";

			if ($compression) {
				gzwrite( $zp, $return );
			} else {
				fwrite( $handle, $return );
			}
			$return= "";

			// insert values
			if ($num_rows) {
				$return= 'INSERT INTO `' . "$table" . "` (";
				$pstm3= $this->query( "SHOW COLUMNS FROM $table" );
				$count= 0;
				$type= array ();
				while ( $rows= $pstm3->fetch( PDO::FETCH_NUM ) ) {
					if (stripos( $rows [1], '(' )) {
						$type [$table] []= stristr( $rows [1], '(', true );
					} else
						$type [$table] []= $rows [1];

					$return.= "`" . $rows [0] . "`";
					$count++;
					if ($count < ($pstm3->rowCount())) {
						$return.= ", ";
					}
				}
				$return.= ")" . ' VALUES';
				if ($compression) {
					gzwrite( $zp, $return );
				} else {
					fwrite( $handle, $return );
				}
				$return= "";
			}
			$count= 0;
			while ( $row= $result->fetch( PDO::FETCH_NUM ) ) {
				$return= "\n\t(";
				for($j= 0; $j < $num_fields; $j++) {
					// $row[$j] = preg_replace("\n","\\n",$row[$j]);
					if (isset( $row [$j] )) {
						// if number, take away "". else leave as string
						if ((in_array( $type [$table] [$j], $numtypes )) && (!empty( $row [$j] )))
							$return.= $row [$j];
						else
							$return.= $this->quote( $row [$j] );
					} else {
						$return.= 'NULL';
					}
					if ($j < ($num_fields - 1)) {
						$return.= ',';
					}
				}
				$count++;
				if ($count < ($result->rowCount())) {
					$return.= "),";
				} else {
					$return.= ");";
				}
				if ($compression) {
					gzwrite( $zp, $return );
				} else {
					fwrite( $handle, $return );
				}
				$return= "";
			}
			$return= "\n\n-- ------------------------------------------------ \n\n";
			if ($compression) {
				gzwrite( $zp, $return );
			} else {
				fwrite( $handle, $return );
			}
			$return= "";
		}
		if ($compression) {
			gzclose( $zp );
		} else {
			fclose( $handle );
		}
	}
}

/* Create database connection */
$database= new MySQLDB();

?>
