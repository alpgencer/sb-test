<?php
/**********************************************************
 Sitebuilder 
 © 2010-2017 Alp H. Gencer
 All rights reserved. No duplication permitted.
 **********************************************************/

// Main query function
function dbQuery($query, $debugflag= 0) {
	global $database;
	if ($debugflag)
		echo "<textarea>$query</textarea>";
	try {
		$stmt= $database->query( $query );
	} catch ( PDOException $ex ) {
		echo "DB query error: " . $ex->getMessage();
		$database->dbLog( $stmt->queryString, "E" );
		return NULL;
	}
	return $stmt;
}

// Return a single row as associative array
function dbGetRow($query, $debugflag= 0) {
	global $database;
	if ($debugflag)
		echo "<textarea>$query</textarea>";
	$result= $database->query( $query );
	if ($result == NULL || ($num_rows= $result->rowCount()) == 0) {
		return NULL;
	} elseif ($num_rows > 1) {
		$database->dbLog( $query, "E" );
		return NULL;
	} else {
		return $result->fetch( PDO::FETCH_ASSOC );
	}
}

// Return a single value as result
function dbGetVal($query, $debugflag= 0) {
	global $database;
	if ($debugflag)
		echo "<textarea>$query</textarea>";
	$result= $database->query( $query );
	if ($result == NULL || ($num_rows= $result->rowCount()) == 0) {
		return NULL;
	} elseif ($num_rows > 1) {
		$database->dbLog( $query, "E" );
		return NULL;
	} else {
		return $result->fetch( PDO::FETCH_NUM ) [0];
	}
}

// Insert row into DB
function dbInsert($table, $fieldsvalues, $debugflag= 0) {
	global $database;
	$fields= "";
	$values= "";
	foreach ( $fieldsvalues as $field => $value ) {
		if ($fields)
			$fields.= ", ";
		$fields.= "`" . $field . "`";
		if ($values)
			$values.= ", ";
		// Handle zero dates
		if (strcmp( $value, "0000-00-00" ) == 0)
			$values.= "NULL";
		else
			$values.= $database->quote( $value );
	}
	$query= "INSERT INTO $table ($fields) VALUES ($values)";
	if ($debugflag)
		echo "<textarea>$query</textarea>";

	$ar= $database->exec( $query );
	if ($ar === FALSE) {
		if ($debugflag)
			echo "DB INSERT error: " . $database->errorInfo() [2];
		$database->dbLog( $query, "E" );
		return NULL;
	}
	$database->dbLog( $query, "I" );
	return $ar;
}

// Update a row
function dbUpdate($table, $fieldsvalues, $where, $debugflag= 0) {
	global $database;
	$varsvalues= "";
	foreach ( $fieldsvalues as $field => $value ) {
		if ($varsvalues)
			$varsvalues.= ", ";
		// Handle zero dates
		if ($value === "0000-00-00")
			$varsvalues.= "`" . $field . "`=NULL";
		else
			$varsvalues.= "`" . $field . "`=" . $database->quote( $value );
	}
	$query= "UPDATE $table SET $varsvalues WHERE $where";
	if ($debugflag)
		echo "<textarea>$query</textarea>";

	$ar= $database->exec( $query );
	if ($ar === FALSE) {
		if ($debugflag)
			echo "DB UPDATE error: " . $database->errorInfo() [2];
		$database->dbLog( $query, "E" );
		return NULL;
	}
	$database->dbLog( $query, "U" );
	return $ar;
}

// Delete rows
function dbDelete($table, $where, $debugflag= 0) {
	global $database;
	$query= "DELETE FROM $table WHERE $where";
	if ($debugflag)
		echo "<textarea>$query</textarea>";

	$ar= $database->exec( $query );
	if ($ar === FALSE) {
		if ($debugflag)
			echo "DB DELETE error: " . $database->errorInfo() [2];
		$database->dbLog( $query, "E" );
		return NULL;
	}
	$database->dbLog( $query, "D" );
	return $ar;
}

?>
