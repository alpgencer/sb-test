<?php
require_once ("../code/constants.php");
require_once ("sb_inc/database.php");

if (isset( $_GET ['sb'] )) {
	$tables= "sb_forms,sb_formfields,sb_lists,sb_listfields,sb_types,sb_tokens";
	$filename= "sb_tables";
} else {
	$tables= "*";
	$filename= DB_NAME;
}
$fname= '../uploads/' . $filename;
$return= $database->backup_tables( $tables, $fname );

/*
 * header('Content-Type: application/octet-stream;');
 * header('Content-Disposition: attachment; filename="'.$filename.'.sql.bz2"');
 * readfile('../uploads/'.$filename.'.sql.bz2');
 */

echo "<p>Export complete: $fname.sql.gz</p>\n";

?>

