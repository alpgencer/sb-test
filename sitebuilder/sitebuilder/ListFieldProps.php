<?php
/**********************************************************
 Sitebuilder 
 © 2010-2015 Alp H. Gencer
 All rights reserved. No duplication permitted.
 **********************************************************/
$oid= sbProcessForm( "sb_listfield", $_GET ['id'] );

if (isset( $_GET ['lid'] ))
	$_POST ['LISTID']= $_GET ['lid'];

sbShowForm( "sb_listfield", $oid );

if ($oid > 0) {
	$q= "SELECT LISTID FROM sb_listfields WHERE ID=$oid";
	$lid= dbGetVal( $q );
	sbLinkToPage( "ListProps", "id=$lid" );
}
sbLinkToPage( "ListAdmin" );

?>