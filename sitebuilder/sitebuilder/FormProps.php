<?php
/**********************************************************
 Sitebuilder 
 © 2010-2015 Alp H. Gencer
 All rights reserved. No duplication permitted.
 **********************************************************/
$oid= sbProcessForm( "sb_form", $_GET ['id'] );

if (isset( $_POST ['subdelete'] )) {
	dbDelete( "sb_formfields", "FORMID=$oid" );
	return;
}

sbShowForm( "sb_form", $oid );

if ($oid > 0) {
	sbShowList( "sb_formfield", "FORMID=$oid", "FormFieldProps", "id=$oid" );
	sbLinkToPage( "FormFieldProps", "id=-1&fid=$oid", token( 151 ) ); // Add new form field
}
sbLinkToPage( "FormAdmin" );

?>