<?php
/**********************************************************
 Sitebuilder 
 © 2010-2019 Alp H. Gencer
 All rights reserved. No duplication permitted.
 **********************************************************/
$histid= $_GET ["id"];
$table= $_GET ["table"];
$q= "SELECT * FROM $table WHERE HISTORY_ID=$histid";
$history= dbGetRow( $q );
$otable= explode( "_", $table, 2 ) [0];

if (isset( $_POST ["restore"] )) {
	$entry= $history;
	unset( $entry ["HISTORY_ID"] );
	unset( $entry ["ACTION"] );
	unset( $entry ["ACTIONDATE"] );
	if ($history ["ACTION"] == "U") {
		dbUpdate( $otable, $entry, "ID=" . $entry ["ID"], 1 );
	} elseif ($history ["ACTION"] == "D") {
		dbInsert( $otable, $entry, 1 );
	}
}

$q= "SELECT * FROM $otable WHERE ID=" . $history ["ID"];
$entry= dbGetRow( $q );
echo '<table class="listTable"><tr>';
echo "<tr><th>FIELD</th><th>Previous value</th><th>Current value</th></tr>\n";
foreach ( $history as $key => $value ) {
	echo "<tr><td>$key</td><td>$value</td><td>\n";
	if (isset( $entry [$key] ) && $entry [$key] != $value)
		echo $entry [$key];
	echo "</td></tr>\n";
}
echo "</table>\n";

echo '<form action="" method="post" enctype="multipart/form-data">';
echo '<input  class="button alert round" type="submit" name="restore" value="Restore entry" onClick="javascript: return confirm(\'Are you sure to restore?\');">';
echo '</form>';

sbLinkToPage( "HistoryDisplay", "table=$table" );
?>