<?php
/**********************************************************
 Sitebuilder 
 © 2010-2015 Alp H. Gencer
 All rights reserved. No duplication permitted.
 **********************************************************/
$oid= sbProcessForm( "sb_list", $_GET ['id'] );

if (isset( $_POST ['subdelete'] )) {
	dbDelete( "sb_listfields", "LISTID=$oid" );
	return;
}

sbShowForm( "sb_list", $oid );
if ($oid > 0) {
	sbShowList( "sb_listfield", "LISTID=$oid", "ListFieldProps", "id=$oid" );
	sbLinkToPage( "ListFieldProps", "id=-1&lid=$oid", token( 153 ) ); // Add new column
}
sbLinkToPage( "ListAdmin" );

?>