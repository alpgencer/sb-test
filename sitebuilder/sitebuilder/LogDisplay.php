<?php
/**********************************************************
 Sitebuilder 
 © 2010-2015 Alp H. Gencer
 All rights reserved. No duplication permitted.
 **********************************************************/
sbLinkToPage( "SearchPage", "list=sb_log&field=QUERY" );

$id= $_SESSION ['ID'];
if (isset( $_GET ['q'] ))
	$q= $_GET ['q'];
else
	$q= "(TYPE='E')";
echo '<ul class="sub-nav">';
echo "<li><a href=\"?p=$page&q=(TYPE='E')\">Errors</a></li>\n";
echo "<li><a href=\"?p=$page&q=(TYPE='D')\">Deletes</a></li>\n";
echo "<li><a href=\"?p=$page&q=(TYPE='I')\">Inserts</a></li>\n";
echo "<li><a href=\"?p=$page&q=(UID<>$id)\">Non-admin</a></li>\n";
echo "<li><a href=\"?p=$page&q=(1)\">All</a></li>\n";
echo "</ul>\n";

sbShowList( "sb_log", $q, "LogEntry" );
sbLinkToPage( "LogClear" );

?>