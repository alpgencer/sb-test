<?php
/**********************************************************
 Sitebuilder 
 © 2010-2019 Alp H. Gencer
 All rights reserved. No duplication permitted.
 **********************************************************/
$q= "SHOW TABLES LIKE '%_history'";
$result= dbQuery( $q );
echo '<ul class="sub-nav">';
foreach ( $result as $row ) {
	$tt= $row [0];
	echo "<li><a href=\"?p=$page&table=$tt\">$tt</a></li>\n";
}
echo "</ul>\n";

if (isset( $_GET ["table"] )) {
	$table= $_GET ["table"];
	sbShowList( $table, "1", "HistoryEntry&table=$table", "table=$table" );
}

?>