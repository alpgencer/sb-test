<?php
/**********************************************************
 Sitebuilder 
 © 2010-2015 Alp H. Gencer
 All rights reserved. No duplication permitted.
 **********************************************************/
$oid= sbProcessForm( "sb_formfield", $_GET ['id'] );

if (isset( $_GET ['fid'] ))
	$_POST ['FORMID']= $_GET ['fid'];

sbShowForm( "sb_formfield", $oid );

if ($oid > 0) {
	$q= "SELECT FORMID FROM sb_formfields WHERE ID=$oid";
	$fid= dbGetVal( $q );
	sbLinkToPage( "FormProps", "id=$fid" );
}
sbLinkToPage( "FormAdmin" );

?>