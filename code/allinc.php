<?php
/**********************************************************
 Sitebuilder 
 © 2010-2015 Alp H. Gencer
 All rights reserved. No duplication permitted.
 **********************************************************/
require_once ("code/constants.php");
// Sitebuilder
require_once ("sitebuilder/sb_inc/database.php");
require_once ("sitebuilder/sb_inc/query.php");
require_once ("sitebuilder/sb_inc/sitebuilder_inc.php");
require_once ("sitebuilder/sb_inc/forms.php");
require_once ("sitebuilder/sb_inc/lists.php");

// Add further includes below

?>
