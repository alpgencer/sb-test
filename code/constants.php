<?php
/**********************************************************
 Sitebuilder 
 © 2010-2015 Alp H. Gencer
 All rights reserved. No duplication permitted.
 **********************************************************/

// Define your default page here
define( "PAGE_DEFAULT", "sitebuilder" );

/* Main site definitions */
define( "WEB_ADDRESS", "http://www.avekon.org" );
date_default_timezone_set( 'Europe/Istanbul' );
define( "TOKENIZED", 0 );
define( "PRIMARY_LANGUAGE", "en" );
/* Login related definitions */
define( "PAGE_LOGIN", "mysite" );
define( "TIMEOUT_SECONDS", 7200 );

// ========== Database connection information ==========
define( 'DB_SERVER', "localhost" );
define( 'DB_NAME', "sb-test" );
define( 'DB_USER', "sb-test" );
define( 'DB_PASS', "sb-test" );

// ===========CONSTANTS=============================

// Admin_levels
define( "A_DEFAULT", 0 );
define( "A_SEK", 8 );
define( "A_ADMIN", 9 );

// Menu types
define( "MENU_DROPDOWN", 10 );
define( "MENU_TOP", 11 );
define( "MENU_LEFT", 12 );
define( "MENU_NONE", 13 );

?>

