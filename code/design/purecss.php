<?php
/**********************************************************
 Lodge Website 
 © 2015 Alp H. Gencer
 All rights reserved. No duplication permitted.
 **********************************************************/
function designCSS() {
	echo '
		<link rel="stylesheet" href="css/foundation.css">
		<link rel="stylesheet" href="css/custom.css">
		<meta name="viewport" content="width=device-width, initial-scale=0.8">
   ';
}
function designPage() {
	global $site, $page;
	if (isset( $_SESSION ["ID"] ))
		$name= getName( $_SESSION ["ID"] );
	else
		$name= "";

	// ---- Navigation bar --------------
	echo '
	<ul class="topnav">
		<li class="title"><a href=".">Sitebuilder Test</a></li>
';
	sbShowSites( $site );
	echo "</ul>\n";

	// ---- Main section -------------------

	showLeftPane( $site );
	echo '<div class="content">';
	showMidPane( $site, $page );
	echo '</div>';

	echo '
<div class="bottom"> 
';
	showImpressum();
	echo '
</div>
';
	// ---- End page ------------------------------
}
function showLeftPane($site) {
	echo '
		<ul class="sidenav"> 
	';
	sbShowMenu( $site );
	echo "</ul>\n";
}
function showMidPane($site, $page) {
	// showHeader($site);
	sbShowPage( $site, $page );
}
function showImpressum() {
	echo '&copy; Design: Alp Gencer';
}

?>

