<?php
/**********************************************************
 International Conference on Eurasian Economies Website 
 © 2010-2015 Alp H. Gencer
 All rights reserved. No duplication permitted.
 **********************************************************/
// Start session
session_start();

// Load all includes
require_once ("code/allinc.php");

// Timeout length
if (isset( $_SESSION ['LAST_ACTIVITY'] ) && (time() - $_SESSION ['LAST_ACTIVITY'] > TIMEOUT_SECONDS)) {
	// last request was more than 30 minutes ago
	session_unset(); // unset $_SESSION variable for the run-time
	session_destroy(); // destroy session data in storage
	session_start(); // and start new session
}
$_SESSION ['LAST_ACTIVITY']= time(); // update last activity time stamp

// Set language cookie
if (!isset( $_COOKIE ["lang"] )) {
	setcookie( "lang", PRIMARY_LANGUAGE, mktime( 0, 0, 0, 12, 31, 2030 ), "/" );
	$lang= PRIMARY_LANGUAGE;
} else {
	$lang= $_COOKIE ["lang"];
}

// Set page, site and path
if (isset( $_GET ['p'] ) && (sbGetPageID( $_GET ['p'] ) != NULL))
	$page= $_GET ['p'];
else
	$page= PAGE_DEFAULT;
$site= sbGetSite( $page );
$q= "SELECT TAG from sb_pages WHERE ID=$site";
$path= dbGetVal( $q );
require_once ("code/$path/header.php");

if (isset( $_SESSION ['ID'] ))
	$adminlevel= getAdminLevel( $_SESSION ['ID'], $CONF );
else
	$adminlevel= A_DEFAULT;

?>

<!DOCTYPE HTML PUBLIC>

<head>
<title>Sitebuilder Test Site</title>

<meta name="author" content="Alp Gencer">
<meta name="description" content="">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">

<?php
sbScriptIncludes();
require_once ("code/design/purecss.php");
designCSS();
?>
<style type="text/css">
</style>

<script src="sitebuilder/livesearch/livesearch.js"></script>

</head>
<body style="background-color: <?php echo siteColor() ?> ;" >

<?php
// Debug here
designPage();
?>
 
</body>
</html>

